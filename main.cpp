#pragma warning(disable:4996)
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include "Functions.h"
#include "GlobalVariables.h"
#include <string>

/**********************Placement des bateaux***********************************/
void place()
{
    int xcoor;  /*Coordonn�es X d'un bateau*/
    int ycoor;  /*Coordonn�es Y d'un bateau*/
    int nbOfBoat;
    srand((unsigned)time(&t)); 
    std::cout << "Set the grid layout [2 to 9] :\n";  /*Dimensionnement de la grille*/
    std::cin >> yAxis;
    xAxis = yAxis;


    if (yAxis < 2 || yAxis>9)                                     /*V�rification des dimensions*/
    {
        while (yAxis < 2 || yAxis>9)
        {
            std::cout << "Incorrect size.\n";
            std::cout << "Please type a size between 2 and 9 :\n";
            std::cin >> yAxis;
            xAxis = yAxis;
        }
    }

    for (i = 1; i <= yAxis; i++)                              /*Creation de la grille de point des bateaux*/
    {
        for (j = 1; j <= xAxis; j++)
        {
            solutionGrid[i][j] = '.';
        }
    }

    for (i = 1; i <= yAxis; i++)                              /*Creation de la grille de point du jeux*/
    {
        for (j = 1; j <= xAxis; j++)
        {
            gameGrid[i][j] = '.';
        }
    }
    std::cout << "Grid created.\n";

    nbOfBoat = xAxis * yAxis / 6;                                  /*G�neration des bateaux pour 1/6 des grilles*/

    if (nbOfBoat < 2)                                      
    {
        nbOfBoat = 2;
    }
    if (nbOfBoat > 9)                                    
    {
        nbOfBoat = 10;
    }
    /*G�neration des  coordonn�es des bateaux*/
    for (i = 1; i <= nbOfBoat; i++)
    {
        xcoor = (rand() % yAxis) + 1;
        ycoor = (rand() % xAxis) + 1;

        while (solutionGrid[xcoor][ycoor] == 'X')                   /*V�rification de risque de doublon*/
        {
            xcoor = (rand() % yAxis) + 1;
            ycoor = (rand() % xAxis) + 1;
        }
        solutionGrid[xcoor][ycoor] = 'X';
    }
    std::cout << "Boats created.\n";
    system("PAUSE");
}

/***************Solution de la partie****************/
void solution()
{
    std::cout << "SOLUTION:\n"" ";           
    for (i = 1; i <= yAxis; i++)             /*R�cup�ration des coordonn�es en lettres*/
    {
        coordToChar = 'A' + i - 1;
        std::cout << "   " << coordToChar;
    }

    for (i = 1; i <= yAxis; i++)           /*Affichage de la table solution avec les bateaux*/
    {
        std::cout << "\n" << i;

        for (j = 1; j <= xAxis; j++)
        {
            std::cout << "   " << solutionGrid[i][j];
        }
        std::cout << "\n";
    }
    system("PAUSE");
}

/********************************D�marrer une partie**********************************/
void play()
{
    int inputXcoor;      /*Coordonn�e X pour jouer*/
    int inputYcoor;      /*Coordonn�e Y pour jouer*/
    int nbBoats;        
    int nbHit;
    int nbMissed;       
    char coordLetter;  /*Coordonn�e X en charactere pour jouer*/
    if (currentPlayer != 1 && currentPlayer != 2)  /*Si c'est une nouvelle partie, le joueur qui commence par d�faut est le #1*/
    {
        currentPlayer = player1;
    }


    for (i = 1; i <= 9; i++)                             /*Cr�ation d'un tableau comparatif Lettre & Chiffre*/
    {
        compareTable[i][1] = i;
        compareTable[i][2] = 'A' + i - 1;
    }

    do
    {
        nbBoats = 0;                         
        nbHit = 0;
        nbMissed = 0;

        for (i = 1; i <= yAxis; i++)         /*R�cup�ration du nombre total de bateaux initialement pr�sents, ainsi que du nombre touch�s et loup�s*/
        {
            for (j = 1; j <= xAxis; j++)
            {
                if (solutionGrid[i][j] == 'X')           
                {
                    nbBoats = nbBoats + 1;
                }
                if (gameGrid[i][j] == '+')           
                {
                    nbHit = nbHit + 1;
                }
                if (gameGrid[i][j] == 'O')       
                {
                    nbMissed = nbMissed + 1;
                }
            }
        }
        system("CLS");

        std::cout << "Press Q to leave.\n\n";
        std::cout << nbBoats << " Boat. \n";                             /*Affichage de l'�tat actuel de la grille*/
        std::cout << nbHit << " Boat hit.\n";
        std::cout << nbMissed << " Boat sunken.\n\n"" ";

        if (nbBoats != nbHit)             /*Verification du nombre de bateaux touch�s pour arr�ter la partie*/
        {
            for (i = 1; i <= yAxis; i++)              /*Boucle pour afficher les coordonne en lettre*/
            {
                coordToChar = 'A' + i - 1;
                std::cout << "   " << coordToChar << "";
            }
            /*Boucle pour afficher la table de jeux*/
            for (i = 1; i <= yAxis; i++)
            {
                std::cout << "\n" << i;
                for (j = 1; j <= xAxis; j++)
                {
                    std::cout << "   " << gameGrid[i][j];
                }
                std::cout << "\n";
            }
            std::cout << "\nCurrent score :\nPlayer #" << player1 << ": " << player1Score << " points.\nPlayer #" << player2 << ": " << player2Score << " points.\n";
            std::cout << "\nIt's now your turn, player #" << currentPlayer << "!\nType the desired coordinates :\n";
            std::cin >> coordLetter;                      /*Verification des coordonne si on demande de Quitter ou non*/

            if (coordLetter != 'Q')                     /*Tant que Q n'est pas saisi, la partie continue*/
            {
                std::cin >> inputXcoor;

                for (i = 1; i <= yAxis; i++)                   /*Convertion de la lettre en chiffre*/
                {
                    if (coordLetter == compareTable[i][2])
                    {
                        inputYcoor = compareTable[i][1];
                    }
                }

                if (inputXcoor > 0 && inputYcoor > 0 && inputXcoor <= yAxis && inputYcoor <= xAxis && coordLetter <= 'I')  /*Si les coordonne sont correcte, le tir est lanc�. Sinon on affiche une erreur*/
                {
                    if (solutionGrid[inputXcoor][inputYcoor] == '.')        /*Si aucun bateau, alors loup�*/
                    {
                        gameGrid[inputXcoor][inputYcoor] = 'O';
                        std::cout << "Missed!\n";
                    }
                    else
                    {
                        if (solutionGrid[inputXcoor][inputYcoor] == 'X')      /*Si bateau, alors touch�-coul�*/
                        {
                            gameGrid[inputXcoor][inputYcoor] = '+';
                            std::cout << "Hit!\n";
                            if (currentPlayer == player1) 
                            {
                                player1Score++;
                            }
                            else {
                                player2Score++;
                            }
                        }
                    }
                    if (currentPlayer == player1) 
                    {
                        currentPlayer = player2;
                    }
                    else
                    {
                        currentPlayer = player1;
                    }
                }
                else                                     /*Si erreur, la donn�e soit �tre ressaisie*/
                {
                    std::cout << "Incorrect choice, please try again\n";
                }

                system("PAUSE");
            }
        }
        else                                   /*Lorsqu'il ne reste aucun bateau � couler*/
        {
            std::cout << "\nCongrats! You've destroyed all boats.\n" << "Final score :\nPlayer #" << player1 << ": " << player1Score << " points.\nPlayer #" << player2 << ": " << player2Score << " points.\n\n\n";
            if (player1Score > player2Score)
            {
                std::cout << "Player #1 Wins ! Player #2, you suck.";
            }
            else if (player2Score > player1Score)
            {
                std::cout << "Player #2 Wins ! Player #1, you suck.";
            }
            else if (player1Score == player2Score)
            {
                std::cout << "Aaaaand.... It's a tie ! Nobody won.";
            }
            std::cout << "\nPlease reset the game.\n\n";
            coordLetter = 'Q';                       /*Fermeture automatique de la partie si aucun bateau restant*/
            system("PAUSE");
        }
    } while (coordLetter != 'Q');                /*Tant que la r�ponse est diff�rente de Quitter, la partie continue*/
}

/*****************************Sauvegarde de la partie**************************/
void save()
{
    /*Sauvegarde du score des joueurs*/
    FILE* scoreSave;
    scoreSave = fopen("./score.txt", "w");
    if (scoreSave == NULL)
    {
        std::cout << "File not found: score.txt";
    }
    else
    {
        fprintf(scoreSave, "%d\n", player1Score);
        fprintf(scoreSave, "%d\n", player2Score);
        fprintf(scoreSave, "%d", currentPlayer);                      /*Sauvegarde du joueur courant*/
        fclose(scoreSave);
        std::cout << "Scores successfully saved!\n";
    }

    /*Sauvegarde des grilles de solution et de jeu*/
    FILE* gameSave;                               
    gameSave = fopen("./save.txt", "w");    
    if (gameSave == NULL)                         
    {
        std::cout << "File not found: save.txt\n";
    }
    else
    {   /*Solution*/
        for (i = 1; i <= yAxis; i++)                  /*Parcours de la grille et �criture des lignes*/
        {
            for (j = 1; j <= xAxis; j++)
            {
                fprintf(gameSave, "%c", solutionGrid[i][j]);
            }
            fprintf(gameSave, "\n");
        }
        /*Jeu*/
        for (i = 1; i <= yAxis; i++)                   /*Parcours de la grille et �criture des lignes*/
        {
            for (j = 1; j <= xAxis; j++)
            {
                fprintf(gameSave, "%c", gameGrid[i][j]);
            }
            fprintf(gameSave, "\n");
        }
        fclose(gameSave);                            
        std::cout << "Game successfully saved!\n";
    }
    system("PAUSE");
}

/*****************************Chargement de la partie**************************/
void load()
{   
    /*Chargement du score des joueurs*/
    FILE* scoreSave;
    scoreSave = fopen("./score.txt", "r");
    if (scoreSave == NULL)
    {
        std::cout << "File not found: score.txt\n";
    }
    else
    {
        std::fstream infile("score.txt");

        if (infile.good())                                             /*R�cup�ration des scores et du joueur courant enregistr�s*/
        {
            std::string savedP1Score;
            std::string savedP2Score;
            std::string savedCurPlayer;
            std::getline(infile, savedP1Score);
            player1Score = std::stoi(savedP1Score);

            for (i = 0; i < 1; i++)
            {
                std::getline(infile, savedP2Score);
            }
            player2Score = std::stoi(savedP2Score);

            for (i = 0; i < 2; i++)
            {
                std::getline(infile, savedCurPlayer);
            }
            currentPlayer = std::stoi(savedCurPlayer);
        }
    }
    fclose(scoreSave);


    /*Chargement de la grille de jeu*/
    char varcb; /*Nombre de chars dans la save*/
    i = 0; j = 0;  
    FILE* gameSave;                      

    gameSave = fopen("./save.txt", "r");

    if (gameSave == NULL)                         
    {
        std::cout << "File not found: save.txt\n";
    }
    else
    {
        while (feof(gameSave) == false)              /*Tant que le fichier n'a pas �t� enti�rement lu*/
        {
            i = i + 1;                                  /*R�cup�ration du nombre de chars*/
            fscanf(gameSave, "%c ", &varcb);
        }
        yAxis = sqrt(i / 2);                            /*Pour les axes X et Y, Calcul de la racine carr� de la moiti� des chars pr�sents*/
        xAxis = sqrt(i / 2);                            /*Obtention des dimensions de la grille sauvegard�e.*/

        fclose(gameSave);                           

        /*Chargement de la grille de solution*/
        gameSave = fopen("./save.txt", "r");

        for (i = 1; i <= yAxis; i++)                        /*Lecture de la grille de solution*/
        {
            for (j = 1; j <= xAxis; j++)
            {
                fscanf(gameSave, "%c", &solutionGrid[i][j]);
            }
            fscanf(gameSave, "\n");
        }

        for (i = 1; i <= yAxis; i++)                        /*Lecture de la grille de jeu*/
        {
            for (j = 1; j <= xAxis; j++)
            {
                fscanf(gameSave, "%c", &gameGrid[i][j]);
            }
            fscanf(gameSave, "\n");
        }
        fclose(gameSave);                             /*Fermeture du fichier Score.txt*/
        std::cout << "Game successfully loaded!\n";
    }
    system("PAUSE");
}

/************************Menu principal du jeu*********************/
void main()
{
    char choice; 
    menu();
    std::cout << "\nYour choice :\n";
    std::cin >> choice;
    while (choice != '0')    
    {
        switch (choice)       
        {
        case '1': place();
            break;
        case '2': play();
            break;
        case '3': solution();
            break;
        case '4': save();
            break;
        case '5': load();
            break;
        default: std::cout << "Error while doing your choice.\n";  
            system("PAUSE");
        }
        system("CLS");          /*Effacement de l'�cran � chaque fin de proc�dure, puis affichage du menu*/
        menu();
        std::cout << "\nYour choice : \n";
        std::cin >> choice;
    }
    system("PAUSE");
}
